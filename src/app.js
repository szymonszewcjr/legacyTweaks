
import {log, logVerbose} from "./log.js"
import {tryAppendLikesSongsHyperlink} from "./appendLikedSongs";
import {injectRefreshUIButton} from "./injectRefreshButton";
import {setupSettings} from "./settings";
import { sidebarResizing } from "./sidebarResizing.js";
import { applyStyle } from "./dynamicStyling.js";
import { waitForElm } from "./waitForElm.js";
import { enableLyricsFix } from "./lyricsFix.js";
import { checkIfUsedWithCompatibleVersions, compatibleVersions } from "./compatibleVersion.js";



async function main() {
  while (!Spicetify?.showNotification) {
    await new Promise(resolve => setTimeout(resolve, 100));
  }
  while (!Spicetify.Platform.PlatformData.event_sender_context_information.client_version_string) {
    await new Promise(resolve => setTimeout(resolve, 100));
  }

  if(!checkIfUsedWithCompatibleVersions(Spicetify.Platform.PlatformData.event_sender_context_information.client_version_string)){

    if(JSON.parse(Spicetify.LocalStorage.get('LegacyLook-hasDisabledCompatibilityWarning')) != true){
       Spicetify.showNotification(`
        LegacyLook.js may not be compatible with this version of Spotify.
        This extension is compatible with Spotify versions ${compatibleVersions.join(", ")}.
        You are using ${Spicetify.Platform.PlatformData.event_sender_context_information.client_version_string}.
        If extension doesn't work, you may need to install any of the versions listed above.
        If it works on any other version, let me know,
        and I will add it to the compatibility list.
        You can disable this message in settings.
        `,0,15000)
    }else{
        logVerbose("Suppressed compatibility warning!");
    }



   } else{
    Spicetify.showNotification("LegacyTweaks.js is active...",0,500);
  }




log("LegacyTweaks.js v0.3 starting up...");
log(`Developed on: Spotify for Windows; 1.2.10.760.g52970952 Spicetify v2.20.3`)
log("May not work on newer Spotify/Spicetify releases...")



waitForElm(".Root__now-playing-bar").then(()=>{ //wait for playing bar to exist
    logVerbose("Playbar exists, waiting for sidebar to exist...")


    setupSettings();

    applyStyle();


    waitForElm(`#spicetify-sticky-list`).then(() => { //wait for sidebar to exist
        logVerbose("Sidebar exists, waiting for navbtns to exist...")
        waitForElm(`.player-controls__left > button:nth-of-type(2)`).then(()=>{//wait for "GO BACK" button to exist
            logVerbose("Assuming spotify is fully loaded...")
            fullyLoaded();
        })
    })
})







const fullyLoaded = () => {



    
    waitForElm(`.main-topBar-historyButtons`).then(el=>{
        el.prepend(injectRefreshUIButton())})
    

    logVerbose("Appended a refresh button to navbuttons...")


    



    waitForElm(`#spicetify-sticky-list`).then((stickyList)=>{
        tryAppendLikesSongsHyperlink(stickyList); 
    })

        sidebarResizing();

        let settingsObject = JSON.parse(Spicetify.LocalStorage.get('LegacyLook-settingsObject'));
        if(settingsObject["fixLyricsNotAutoscrolling"]){
            enableLyricsFix();
        }
        log("Loaded correctly... No abnormalities detected...")

}
}
export default main;

