import { logVerbose } from "./log";

export const compatibleVersions = ["1.2.9.743", '1.2.10.760'];
export const checkIfUsedWithCompatibleVersions = (version) => {
    logVerbose("Currently running: ", version);
    logVerbose("is compatible:",  compatibleVersions.includes(version));

    return  compatibleVersions.includes(version);
};