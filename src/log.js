var log = function (){     
    var args = Array.prototype.slice.call(arguments);
    args.unshift("[legacyLook.js]: ");
    console.log.apply(console, args);
}
var logVerbose = function (){     
    var args = Array.prototype.slice.call(arguments); 
    args.unshift("[legacyLook.js]: "); 
    console.debug.apply(console, args);
}
export {log,logVerbose};