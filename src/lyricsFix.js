import { log, logVerbose } from "./log";

const enableLyricsFix = () => {
    logVerbose("Lyrics Fix starting...")
    

        if(global.legacyTweaks_lyricsFixInterval){
            logVerbose("Lyrics fix timer already running...");
            return;
        }
        logVerbose("Lyrics fix enabled");

        global.legacyTweaks_lyricsFixInterval = setInterval(()=>{
            if(document.querySelector(`.lyrics-lyricsContent-active`)){
                if(document.querySelector(`.lyrics-lyricsContent-active`).checkVisibility()){
                    document.querySelector(`.lyrics-lyricsContent-active`).scrollIntoViewIfNeeded();
                    logVerbose("Lyrics fix: scrolling...");
                };

                
            }
        },2000);
    


        
}

const disableLyricsFix = () => {


    if(legacyTweaks_lyricsFixInterval in global){
        clearInterval(global.legacyTweaks_lyricsFixInterval);
        global.legacyTweaks_lyricsFixInterval = null;
        log("Lyrics fix disabled");
    }else{
        log("Lyrics fix already disabled");
    }
}

export {disableLyricsFix,enableLyricsFix}