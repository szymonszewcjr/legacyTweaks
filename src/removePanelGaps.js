import { waitForElm } from "./waitForElm.js";
import { log, logVerbose } from "./log";


const removePanelGaps = () => {
    waitForElm(`.Root`).then((element) => {element.style.setProperty("--panel-gap", "0px", "important");})
    waitForElm(`[aria-label="Main"]`, elm => elm.style.gap = "0px")
    logVerbose("Removed gaps between panels...")
}

const revertPanelGaps = () => {
    waitForElm(`.Root`).then((element) => {element.style.setProperty("--panel-gap", "8px", "important");})
    waitForElm(`[aria-label="Main"]`, elm => elm.style.gap = "8px") 
    logVerbose("Adding back gaps between panels...")
}



const updatePanelGapsState = () => {
    let settingsObject = JSON.parse(Spicetify.LocalStorage.get('LegacyLook-settingsObject'));
    if (settingsObject["removePanelGaps"]) {
        removePanelGaps();
    } else {
        revertPanelGaps();
    }

}

export { updatePanelGapsState }