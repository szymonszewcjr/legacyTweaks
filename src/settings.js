import { SettingsSection } from "spcr-settings";
import { changedMaxWidthSignal } from "./sidebarResizing";
import { updateStyleElement } from "./dynamicStyling";
import { updatePanelGapsState } from "./removePanelGaps";
import { log, logVerbose } from "./log";
import { disableLyricsFix, enableLyricsFix } from "./lyricsFix";
import { waitForElm } from "./waitForElm";
const defaultSettingsObject =()=> {
  let settingsObject= {
        "requiredTweaks": true,
        "playlistTitleShift12px": true,
        "roundedPlayButtons": true,
        "roundedTopNavbuttons": true,
        "fullSizeCoverart": true,
        "removePanelGaps": true,
        "changePlaybarColor": true,
        "rootBackgroundColor": true,
        "removeRoundedPanelCorners": true,
        "removePlaylistCoverArt": true,
        "squareChips": true,
        "removeSidebarListEntryRoundedCorners": false,
        "playlistEntryUnderscored": true,
        "removeTrackListRoundedCorners": true,
        "singleLineTracklist": true,
        "oldPlayButton": true,
        "removeExpandLibraryArrowButton": true,
        "carouselRoundedRectangles": true,
        "removeBlankSpaceBetweenMyLibraryAndLikedSongs": true,
        "sidebarLibraryUnderscored": true,
        "removeRoundedCornersSpotifyConnect": true,
        "removeRoundedCornersNavbuttons":true,
        "fixLyricsNotAutoscrolling":false,
        

    }
    return settingsObject;
}


/**
 * TODOS:
 * Add input for single line playlist heigth;
 * Add switch for refresh button
 * 
 * 
 */
const setupSettings = () => {
    global.legacyTweaks_lyricsFixInterval = null;
    let settingsFromStorage = JSON.parse(Spicetify.LocalStorage.get('LegacyLook-settingsObject')) 
    let settingsObject = {}// 

    const resetSettings = () => {
        Spicetify.LocalStorage.set('LegacyLook-settingsObject', null);
        
    }


    const saveObject = () => {
        Spicetify.LocalStorage.set('LegacyLook-settingsObject', JSON.stringify(settingsObject));
        updateStyleElement();
    }


    if (settingsFromStorage == null) { //if object doesn't exist in local storage, create one and save it.
        settingsObject = defaultSettingsObject();

        log("Settings not available in local storage, most likely first run.")

        saveObject();
        Spicetify.showNotification("Hello! Please visit Spotify settings to configure the extension to your liking.",true , 10000);
        logVerbose("Current settings are: ", settingsObject)
        
        
    }else{
        settingsObject = settingsFromStorage;
    } 

    const settings = new SettingsSection("LegacyLook Tweak settings", "LegacyLook-settings");



    settings.addButton("button.factoryReset", "Reset to defaults, recommended after updating.","Factory Reset",()=>{
        resetSettings();
        
        settings.rerender();
        Spicetify.showNotification(`Reset LegacyTweaks settings to defaults! Reloading shortly...`);
        setTimeout(()=>{location.reload()},750)
        
    })

            
    

    let defaultWidth = 420;
    settings.addInput("left-sidebar-max-width", "Left Sidebar Max Width in pixels (420px by default)",
     Spicetify.LocalStorage.get('LegacyLook-sidebarMaxWidth')? Spicetify.LocalStorage.get('LegacyLook-sidebarMaxWidth'): defaultWidth, //if entry exists, use it, else use default
      () => {

        let inputWidth = parseInt(settings.getFieldValue("left-sidebar-max-width"));
        if (inputWidth < 200 || inputWidth > 1000) {
            inputWidth = 420;
            Spicetify.showNotification("Value must be greater or equal than 200 and less or equal to 1000!");

        } else {

            Spicetify.LocalStorage.set("LegacyLook-sidebarMaxWidth", inputWidth);
            changedMaxWidthSignal(parseInt(settings.getFieldValue("left-sidebar-max-width")));
        }

    });
   
    settings.addButton("button.set-sidebar-max-width-default", `Reset width to default (${defaultWidth}px)`, "RESET", () => {

        settings.setFieldValue("left-sidebar-max-width", `${defaultWidth}px`)
        Spicetify.LocalStorage.set("LegacyLook-sidebarMaxWidth", defaultWidth);
        changedMaxWidthSignal(parseInt(settings.getFieldValue("left-sidebar-max-width")));
        settings.rerender();
        Spicetify.showNotification(`Reset to default: ${defaultWidth}px`);
    });




    settings.addToggle(
        "hasDisabledCompatibilityWarning",
        "Disable Compatibility warning at startup",
        Spicetify.LocalStorage.get('LegacyLook-hasDisabledCompatibilityWarning'),
         (e) => {Spicetify.LocalStorage.set('LegacyLook-hasDisabledCompatibilityWarning',e.target.checked)}
         )
    

    settings.addToggle("playlistTitleShift12px",
        "Shifts playlist title by 12px to the right.",
        settingsObject["playlistTitleShift12px"], (e) => {
            settingsObject["playlistTitleShift12px"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("fullSizeCoverart",
        "Cover Art will take up full width of the Sidebar.",
        settingsObject["fullSizeCoverart"], (e) => {
            settingsObject["fullSizeCoverart"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("removePanelGaps",
        "Removes gaps between UI sections, it is meant to be used together with rounded panel corners remover.",
        settingsObject["removePanelGaps"], (e) => {
            settingsObject["removePanelGaps"] = e.target.checked;
            saveObject();
            updatePanelGapsState();
        });

    settings.addToggle("removeRoundedPanelCorners",
        "Removes rounded corners of UI panels, it is meant to be used together with panel gaps remover.",
        settingsObject["removeRoundedPanelCorners"], (e) => {
            settingsObject["removeRoundedPanelCorners"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("changePlaybarColor",
        "Changes playbar color to gray, just like 2017 Spotify App.",
        settingsObject["changePlaybarColor"], (e) => {
            settingsObject["changePlaybarColor"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("rootBackgroundColor",
        "Changes background color scheme to gray, just like 2017 Spotify App.",
        settingsObject["rootBackgroundColor"], (e) => {
            settingsObject["rootBackgroundColor"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("removePlaylistCoverArt",
        "Hides small covers in tracklists. Cover images will still use data, just won't be shown.",
        settingsObject["removePlaylistCoverArt"], (e) => {
            settingsObject["removePlaylistCoverArt"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("squareChips",
        "Makes selectors just below \"My Library\" square instead od rounded.",
        settingsObject["squareChips"], (e) => {
            settingsObject["squareChips"] = e.target.checked;
            saveObject();
        });
/*
    settings.addToggle("removeSidebarListEntryRoundedCorners",
        "Removes rounded corners from entries in the list below \"My library\". (Currently buggy)",
        settingsObject["removeSidebarListEntryRoundedCorners"], (e) => {
            settingsObject["removeSidebarListEntryRoundedCorners"] = e.target.checked;
            saveObject();
        });
*/

    settings.addToggle("playlistEntryUnderscored",
        "Underscores each tracklist entry, just like 2017 Spotify App did.",
        settingsObject["playlistEntryUnderscored"], (e) => {
            settingsObject["playlistEntryUnderscored"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("sidebarLibraryUnderscored",
        "Underscores each sidebar Library entry.",
        settingsObject["sidebarLibraryUnderscored"], (e) => {
            settingsObject["sidebarLibraryUnderscored"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("removeTrackListRoundedCorners",
        "Removes rounded corners of tracklist entries.",
        settingsObject["removeTrackListRoundedCorners"], (e) => {
            settingsObject["removeTrackListRoundedCorners"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("singleLineTracklist",
        "Tracklist entries take up only single line, TITLE-ARTIST on a single line.",
        settingsObject["singleLineTracklist"], (e) => {
            settingsObject["singleLineTracklist"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("oldPlayButton",
        "Reverts main play button to look like the one 2017 Spotify App had.",
        settingsObject["oldPlayButton"], (e) => {
            settingsObject["oldPlayButton"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("removeExpandLibraryArrowButton",
        "Removes \"Expand Library Arrow\" button. Expanding library is still possible via dragging.",
        settingsObject["removeExpandLibraryArrowButton"], (e) => {
            settingsObject["removeExpandLibraryArrowButton"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("carouselRoundedRectangles",
        "Changes carousel buttons to rounded rectangles, used when selectors below \"My Library\" no longer fit",
        settingsObject["carouselRoundedRectangles"], (e) => {
            settingsObject["carouselRoundedRectangles"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("removeBlankSpaceBetweenMyLibraryAndLikedSongs",
        "Removes a bit of blank space between \"Liked Songs\" and \"My Library\".",
        settingsObject["removeBlankSpaceBetweenMyLibraryAndLikedSongs"], (e) => {
            settingsObject["removeBlankSpaceBetweenMyLibraryAndLikedSongs"] = e.target.checked;
            saveObject();
        });


    settings.addToggle("removeRoundedCornersSpotifyConnect",
        "Removes rounded borders of Spotify Connect bar so it fits nicely.",
        settingsObject["removeRoundedCornersSpotifyConnect"], (e) => {
            settingsObject["removeRoundedCornersSpotifyConnect"] = e.target.checked;
            saveObject();
        });

    settings.addToggle("removeRoundedCornersNavbuttons",
        "No more circular nav buttons.",
        settingsObject["removeRoundedCornersNavbuttons"], (e) => {
            settingsObject["removeRoundedCornersNavbuttons"] = e.target.checked;
            saveObject();
        });

        settings.addToggle("fixLyricsNotAutoscrolling",
        "Fix Lyrics Not Autoscrolling issue#4",
        settingsObject["fixLyricsNotAutoscrolling"], (e) => {
            settingsObject["fixLyricsNotAutoscrolling"] = e.target.checked;
            waitForElm(".lyrics-lyrics-container").then((elm) => {
            if(e.target.checked){
                enableLyricsFix();
            }else{
                disableLyricsFix();
            }
            })

            saveObject();
        });
       



        

    const checkForReact = ()=>{
        if(Spicetify.React) { 
        settings.pushSettings();
    }else{
        setTimeout(checkForReact,50);
    }    
}
 checkForReact();   
    
    

    
}
export { setupSettings }